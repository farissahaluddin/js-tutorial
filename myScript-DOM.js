// DOM JS



window.document.write("<br>document object is property of window object.<br>");

// access elements using class attribute

var hElement = document.getElementById("my-heading");


var pElement = document.getElementsByClassName("my-paragraph");
// alert(pElement);
// alert(pElement[0]);
// alert(pElement[0].innerHTML);

// LOOP THROUGHT ALL PARAGRAPHS

for( var i=0; i< pElement.length; i++) {
    // alert(pElement[i].innerHTML);
}

// accesss element using tag name
var ulElement = document.getElementsByTagName("ul");
// alert(ulElement);
// alert(ulElement[1].innerHTML);

// var liElement = ulElement[1].getElementsByTagName("li");
// alert(liElement[0].innerHTML);


// ACCESS ELEMENTS USING NAME ATTRIBUTE
var naElements = document.getElementsByName("user-name");
// alert(naElements[0].tagName);

// ACCESS ELEMENT USING CSS QUERY
// var pElement = document.querySelector("P");
// alert(pElement.innerHTML);

function doSomething() {
    // alert("You clicked!");
}

// get button by id
var btnElement = document.getElementById("btn");
btnElement.onclick = function() {
    // alert("you just click cuy!");
}

window.onload = function() {
    // alert("The page just finished.");
}


// get heading element by id
var hElement = document.getElementById("my-heading");

hElement.onmouseover = function() {
    if ( this.className == "h-normal") {
        this.className = "h-changed";
    } else {
        this.className = "h-normal";
    }
}

hElement.onmouseover = function() {
    // alert("it just overwrite");
}

// get button by id

var btn2Element = document.getElementById("btn2");
/*
function changeBtnSize() {
    this.style.fontSize = "45px";
}
btn2Element.addEventListener("click", changeBtnSize);
btn2Element.addEventListener("click", function() {
    alert("this not overwrite previous event");
});
*/

function changeBtnSize(event) {

    event.target.style.fontSize = "40px";
    alert(event.type);
    alert(event.target);
}
// btn2Element.addEventListener("click", changeBtnSize);  

