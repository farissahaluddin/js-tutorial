// JS ERROR

try {
    callMe();
}
catch(error) {
    alert("Error Message : " + error.message);
}

// TRY THROW CATCH
var x = "test";

try {
    if( inNan(x)) {
        throw("Not a number");
    }
}
catch(e) {
    alert("Error :" +e );
}


// TRY THROW CATCH FINNALY
var x = "test";

try {
    if( inNan(x)) {
        throw("Not a number");
    }
}
catch(e) {
    alert("Error :" +e );
}
finally {
    document.write("<h1> There is an error but keep going!");
}