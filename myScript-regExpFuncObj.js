//  JS regexp & function Object

// create regular expression using new keyword

var regExp1 = new RegExp("adcb");

// ALTERNATE syntax
/*
var regExp2 = /adcb/;

var strToSearch = "This srere is a a bank name";
var matchFound = regExp2.test(strToSearch);
alert(matchFound);
*/

// FUNCTION OBJECT

var functionAsObject = new Function("a", "b", "return a+b");

var x = functionAsObject(10,25);

alert(x);

var y = function(a,b) {
    return (a+b)
}

alert(y(25,20));