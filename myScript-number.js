// JS number type & number

var n = new Number(15);

document.write(n + "<br>");
document.write("Primitive value : " + n.valueOf() + "<br>");

// CREATE NUMBER 
var n = 15.5;
document.write(n + "<br>");

// CHECK FOR VALID NUMBER
var numberCheck = 20 + "t";
document.write(numberCheck + " is not valid number : " + isNaN(numberCheck) + "<br>");

// TO STRING
var myNumber = 555;
document.write(myNumber.toString() + "<br>");