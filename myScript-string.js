var ss = new String("This is America");
document.write(ss + "<br>");

var s = "This is USA";
document.write(s + "<br>");

// ADD SPECIAL CHARACTERS INTO STRING

var myString = "This is \"vanilla\" ice cream";
document.write(myString + "<br>"); 

var myString = "This is \'vanilla\' ice cream";
document.write(myString + "<br>"); 

var myString = "This is \\vanilla\\ ice cream";
document.write(myString + "<br>"); 


// STRING PROPERTIES AND METHODS
var myQuestion = "Where do you live";

// LENGHT OF STRING
document.write("Lenght : " + myQuestion.length + "<br>");

// INDEX OF
document.write("Index of : " + myQuestion.indexOf("you") + "<br>");

// SBU STRING
document.write(myQuestion.substring(6, 12) + "<br>");

// REPLACE STRING
document.write(myQuestion.replace("live", "go") + "<br>");

// UPPPERCASE
document.write(myQuestion.toUpperCase() + "<br>");  