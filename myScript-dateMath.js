// Javascript bult-in Date & Math objects

/*
var rightNow = new Date();
alert(rightNow); 
*/

// var dt = new Date(58789999999999855);
// alert(dt);
/*
dt = new Date("September 19, 2016");
alert(dt);

dt = new Date(2004, 5, 24, 6, 40, 12, 0);
alert(dt);
*/

/*
var dtm = new Date();

document.write("Date : " + dtm.getDate() + " <br> ");
document.write("Day : " + dtm.getDay() + " <br> ");
document.write("Month : " + dtm.getMonth() + " <br> ");
document.write("Full Year : " + dtm.getFullYear() + " <br> ");
*/

// MATH OBJECT HANDLE OPERATION

var sqr = Math.sqrt(20);
document.write("Square Root : " + sqr + "<br>");

var max = Math.max(10,3,4,22,31);
document.write("Maximmum : " + max + "<br>");

var rnd = Math.round(99.4);
document.write("Rounded : " + rnd + "<br>");