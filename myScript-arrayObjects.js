// JS array Objects
/*
var myColors = new Array("red", "green", "blue");
document.write(myColors[2] + "<br>");

var myColors = ["red", "green", "blue"] ;
document.write(myColors[1] + "<br>");

// modify array element

myColors[0] = "yellow";
document.write(myColors[0] + "<br>");

document.write("semua : " + myColors + "<br>");

// sort an alphabetically

document.write("sort : " + myColors.sort() + "<br>");

// reverse an array

document.write("reverse : " + myColors.reverse() + "<br>");

// Add element using push
myColors.push("Pink");
document.write("tambah 1 : " + myColors + "<br>");

// remove element using pop
myColors.pop("Pink");
document.write("minus 1 : " + myColors + "<br>");

*/


// JOIN TWO ARRAY N MORE
var listNumb1 = [1,2,3];
var listNumb2 = [4,5,6];
var allNumb =  listNumb1.concat(listNumb2);
document.write(allNumb + "<br>");

// LOOP THROUGH ARRAY ELEMENTS
var myCars = ["BMW", "Honda", "Toyota"];
for (var i=0; i < myCars.length; i++) {
    document.write(myCars[i] + "<br>");
}


// ARRAY OBJECTS
var myCars = [
    {model : "BMW", year: 2016},
    {model : "Honda", year: 2019},
    {model : "Mazda", year: 2014}
];

for(var k=0; k < myCars.length; k++) {
    var myCar = "";
    for (var cp in myCars[k]) {
        myCar = myCar+myCars[k][cp];
    }
    document.write(myCar + "<br>");
}