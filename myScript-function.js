// function myFirstFunction() {

//     document.write("Code execution inside function.");
// }
// myFirstFunction();



//_________________________________
// FUNCTION RETURN VALUE

function functionWithParameters(p1,p2) {

    var z = p1 * p2;
    document.write("p1 * p2 = "+z+"<br>");

}
functionWithParameters(10,20);


// ___________________________________
// FUNCTION AS VALUE

function parameterAsFunction( func ) {
    func();
}
parameterAsFunction(helloDunia);

function helloDunia() {
    alert("hello, Dunia! Function passed as an argument.");
}



// ___________________________________
// FUNCTION WITH RETURN STATEMENT

function returnSum (x,y) {
    var z=x+y;
    return z;
}

var result = returnSum(10,20);
document.write("Returned Value : "+ result +"<br>");


// ___________________________________
// FUNCTION AS VARIABLE & ANONYMOUS FUNCTION

function sum(t,c) {
    return (t+c);
}

// assign a function to a variable
var s = sum;
var r = s(10,25);

document.write("Jumlahnya = " +r+ "<br>");

// ___________________________________
// FUNCTION WITH A NAME

var isAdult = function(Age) {
    if (Age >= 18) {
        return "YES";
    } else {
        return "NO";
    }

};
var k = isAdult(17);
document.write("Adult Staus : "+ k);