// JS BOOLEAN TYPE & BOOLEAN OBJ

var b = new Boolean(true);

document.write(b + "<br>");
document.write(b.valueOf() + "<br>");

// CREATE BOOLEAN ASSIGNING TRUE OR FALSE TO A VARIALBLE

var x = true;
var y = false;

var z = x.toString();

document.write(z + "<br>");

// CONDITIONAL STATEMENT

if(true) {
    document.write("Condition with true state.<br>");
}
if(false) {
    document.write("Condition with true state.<br>");
}
if(1) {
    document.write("Condition with 1/true state.<br>");

}
if(0) {
    document.write("Condition with 0/flase state.<br>");
}

if( 2 < 3) {
document.write("2 is less than 3. <br>");
}