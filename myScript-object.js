// Javascript Objects

/*
var person = new Object();
person.firstName = "Faris";
person.lastName  = "sahaluddin";
person.age = 25;
person.height = 5.9;
person.fullName = function() {
    return (person.firstName + " " + person.lastName);
}

alert(person.firstName);
alert(person["age"]);
alert(person.fullName());
*/

// Create an object using an object constructor

/*
function Person (fName, lName, age, hg) {

    this.firstName = fName;
    this.lastName  = lName;
    this.age = age;
    this.height = hg;
    this.fullName = function() {
        return (this.firstName + " " + this.lastName);
    }
}
*/

/*
var myBrother = new Person("Fraussel", "brownee", 25, 5.9);
var mySister   = new Person("lara", "brownee", 15, 5.7);

alert(myBrother.firstName);
alert(myBrother.fullName());

alert(mySister.firstName);
alert(mySister.fullName());
*/


// create an object using an object literal 

/*
var person = new Object();
person.firstName = "Faris";
person.lastName  = "sahaluddin";
person.age = 25;
person.height = 5.9;
person.fullName = function() {
    return (person.firstName + " " + person.lastName);
}

alert(person.firstName);
alert(person["age"]);
alert(person.fullName());

var x = person;
x.firstName = "zohan";

alert(x.firstName);
alert(person.firstName);

delete person.age;
alert(person.age);
*/


var myCar = {
    name : "BMW",
    year: 2016,
    color: "Black"
};

var v = "";

for(var k in myCar ) {
    v= v+myCar[k] + " ";
}
alert(v);


// Nested Objects

var user = {
    name : "rassel",
    age : 25,
    size: {
        top : 98,
        middle: 60,
        button: 90
    }
};

alert(user.name);
alert(user.size.top);