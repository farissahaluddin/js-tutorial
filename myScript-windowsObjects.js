// WINDOWS OBJECT JS

/*
    - window
*/

// GLOBAL VARIABLE

/*
var x = 15;
alert(window.x);

// GLOBAL FUNCTION
function y() {
    document.write("Global function invokde using window object. <br>");
}
window.y();

alert("alert invoke without window object.");
window.alert("alert invoke using window object.");

window.confirm("confirm invoke useing window object");
window.prompt("prompt is invoked using window object.");
*/

// WIDTH & HEIGHT OF BROWSER WINDOW

document.write("Window width : " + window.innerWidth + " in pixels <br>");

document.write("Window height : " + window.innerHeight + " in pixels <br>");

document.write("Window  outer width : " + window.outerWidth + " in pixels <br>");

document.write("Window outer height : " + window.outerHeight + " in pixels <br>");

// SET WINDOW NAME
window.name = "FARIS WINS";
document.write("Window name " + window.name + "<br>");